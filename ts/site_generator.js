"use strict";
var wipe_reasons = {
    "https://dps.report/2HAl-20220321-221540_vg": "ben got tank",
    "https://dps.report/Nnfs-20220314-222116_vg": "pv was not in green during split",
    "https://dps.report/Rt5t-20220606-221603_vg": "pv blinked out of a blue during split 2",
    "https://dps.report/khSr-20220601-193922_vg": "lack of healing in p3 before green",
    "https://dps.report/Xmwx-20220606-221858_vg": "lack of healing followed by missing seeker controll",
    "https://dps.report/A425-20220321-222341_gors": "charged soul was not stopped",
    "https://dps.report/iVIy-20220425-222252_gors": "charged soul was not stopped",
    "https://dps.report/WHft-20220330-202507_sab": "waro died gliding, later wiped to 4 canons up",
    "https://dps.report/z8xg-20220425-222949_sab": "lost 2 ppl to flamewall while ressing after canon downed to Kernan",
    "https://dps.report/0EHg-20220209-204826_sloth": "failed stealth strat at start",
    "https://dps.report/66o3-20220209-205728_sloth": "ewayne dced (?)",
    "https://dps.report/cQIS-20220221-221308_sloth": "failed stealth strat at start",
    "https://dps.report/bfKF-20220214-215443_sloth": "failed stealth strat at start",
    "https://dps.report/Ds8X-20220321-214410_sloth": "shroom 2 died to ben's gs1",
    "https://dps.report/L2hE-20220418-220409_sloth": "shroom 1 died to ewayne's phantasmal blades",
    "https://dps.report/vmNj-20220418-220518_sloth": "shroom 1 died to ewayne's sword 5",
    "https://dps.report/oUOS-20220314-221336_matt": "ben died while placing fire",
    "https://dps.report/MoRX-20220425-220959_matt": "ben's focus 5 absorbed reflect",
    "https://dps.report/uBd7-20220307-215650_kc": "too slow cc",
    "https://dps.report/fKKl-20220328-222603_kc": "too slow cc",
    "https://dps.report/QlCR-20220328-222715_kc": "first statues merged",
    "https://dps.report/iBSv-20220307-221539_xera": "lost riv to gliding, then didn't clear shards",
    "https://dps.report/ucmO-20220303-212514_mo": "gg since cm was not active",
    "https://dps.report/Sh7q-20220303-212634_mo": "ewayne was to close to the edge of the blue",
    "https://dps.report/AKAD-20220303-212853_mo": "issues with statue, multiple people miss blue",
    "https://dps.report/Tf5H-20220307-211743_mo": "issues with statues",
    "https://dps.report/EUwi-20220509-202446_sh": "gg since cm was not active",
    "https://dps.report/7nTU-20220303-202445_qadim": "ben, waro died to reapers of flesh and apocalypse bringer",
    "https://dps.report/y87z-20220303-203322_qadim": "pv died to mace 2, 33% dps check failed",
    "https://dps.report/nUVI-20220303-191553_sabir": "testing stuff",
    "https://dps.report/KaNj-20220425-192141_sabir": "testing stuff",
    "https://dps.report/qFMB-20220221-193919_adina": "Toucan not behind 1st pillar",
    "https://dps.report/YZSn-20220328-195838_adina": "ewayne gged for unknown reason or died to pillars",
    "https://dps.report/qf4w-20220303-193559_adina": "ewayne got tank as qcata",
    "https://dps.report/u9gI-20220303-193736_adina": "dps check failed (76.9%)",
    "https://dps.report/1G5k-20220321-193613_adina": "dps check failed (75.8%)",
    "https://dps.report/ngPQ-20220328-195430_adina": "dps check failed (50.8%)",
    "https://dps.report/QMvG-20220328-201110_adina": "dps check failed (25.4%)",
    "https://dps.report/mbXy-20220328-194919_adina": "dps check failed (76.0%)",
    "https://dps.report/PLhm-20220328-195636_adina": "Ines and Messalina died to wall",
    "https://dps.report/0IUq-20220328-195749_adina": "Waro died to wall",
    "https://dps.report/4TVm-20220328-195034_adina": "slb got tank instead of pv",
    "https://dps.report/qSqM-20220328-195210_adina": "slb got tank instead of pv",
    "https://dps.report/lqxg-20220321-193709_adina": "ewayne wrong build",
    "https://dps.report/LxCC-20220214-193748_adina": "ewayne (tank) died to mines and then wall",
    "https://dps.report/tfbd-20220328-200036_adina": "pv (tank) died to mines and then wall",
    "https://dps.report/EzfA-20220404-194038_adina": "pv (tank) died to mines and then wall",
    "https://dps.report/t9Ue-20220411-194045_adina": "ines got stuck inside the platform",
    "https://dps.report/Avob-20220425-194709_adina": "ines died to wall",
    "https://dps.report/6EbU-20220425-194553_adina": "kerchu's reflect to late, druid knocked in sand"
};
var ALACRITY_BUFF_ID = 30328;
var QUICKNESS_BUFF_ID = 1187;
function sortTable(column_index, table_id) {
    function shouldSwitchFunction(a, b) {
        // if one of the elements does not parse into a float compare strings
        var dateRegex = RegExp("\\d\\d\\.\\d\\d\\.\\d\\d");
        if (dateRegex.test(a) && dateRegex.test(b)) {
            var adata = a.match(/\d\d/g);
            var bdata = b.match(/\d\d/g);
            console.log(10000 * +adata[2] + 100 * +adata[1] + +adata[0], 10000 * +bdata[2] + 100 * +bdata[1] + +bdata[0]);
            return 10000 * +adata[2] + 100 * +adata[1] + +adata[0] > 10000 * +bdata[2] + 100 * +bdata[1] + +bdata[0];
        }
        else if (isNaN(parseFloat(a)) || isNaN(parseFloat(b))) {
            if (a.toLowerCase() > b.toLowerCase()) {
                return true;
            }
            // otherwise sort by numbers
        }
        else {
            if (parseFloat(a) > parseFloat(b)) {
                // If so, mark as a switch and break the loop:
                return true;
            }
        }
        return false;
    }
    var switching, i, x, y, shouldSwitch, dir;
    var switchcount = 0;
    var table = document.getElementById(table_id);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "desc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        var rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[column_index];
            y = rows[i + 1].getElementsByTagName("TD")[column_index];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir === "asc") {
                if (shouldSwitchFunction(x.innerHTML, y.innerHTML)) {
                    shouldSwitch = true;
                    break;
                }
            }
            else if (dir === "desc") {
                if (shouldSwitchFunction(y.innerHTML, x.innerHTML)) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        }
        else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "desc") {
                dir = "asc";
                switching = true;
            }
        }
    }
    // update sort marker
    var headers = table.rows[0].getElementsByTagName("TH");
    for (i = 0; i < headers.length; i++) {
        headers[i].innerHTML = headers[i].innerHTML.replace("▲", "").replace("▼", "");
    }
    headers[column_index].innerHTML += dir == "asc" ? "▲" : "▼";
}
function table_row(cells, extra) {
    var data = "<tr>";
    var i = 0;
    for (var index in cells) {
        data += "<td";
        if (extra && extra[i]) {
            data += " " + extra[i];
        }
        data += ">";
        data += cells[index];
        data += "</td>";
        i++;
    }
    data += "</tr>";
    return data;
}
function dist_viz(data, width, data_limit, style_appendix) {
    var sorted = data.sort(function (a, b) {
        return a - b;
    });
    var len = sorted.length;
    var min = len >= 2 ? sorted[0] : sorted[0] - data_limit / width;
    var max = len >= 2 ? sorted[len - 1] : sorted[0] + data_limit / width;
    var lwr68 = len >= 4 ? sorted[Math.round(0.16 * len - 0.5)] : min;
    var upr68 = len >= 4 ? sorted[Math.round(0.84 * len - 0.5)] : max;
    var lwr95 = len >= 21 ? sorted[Math.round(0.05 * len - 0.5)] : lwr68;
    var upr95 = len >= 21 ? sorted[Math.round(0.95 * len - 0.5)] : upr68;
    var svg = "<div title='Max: ".concat(sorted[len - 1], "'><svg  class='dist-viz' height='20' width='").concat(width, "'>");
    svg += "<line x1='".concat(min * width / data_limit, "' x2='").concat(max * width / data_limit, "' y1='10' y2='10' style='stroke:black;stroke-width:1' />");
    svg += "<rect class='percentile-95-".concat(style_appendix, "' x='").concat(lwr95 * width / data_limit, "' y='6' width='").concat((upr95 - lwr95) * width / data_limit, "' height='8' />");
    svg += "<rect class='percentile-68-".concat(style_appendix, "' x='").concat(lwr68 * width / data_limit, "' y='4' width='").concat((upr68 - lwr68) * width / data_limit, "' height='12' />");
    svg += "</svg></div>";
    return svg;
}
function player_in_log(player) {
    if (player.roles && player.roles.some(function (e) { return e == "tank"; })) {
        return spec_icon_title_class(player.build.elite_spec, player.name + "\n" + player.build.name + "\nD/C: " + player.target_dps + "/" + player.target_cc, "icon-tank");
    }
    else if (player.roles && player.roles.some(function (e) { return e == "special"; })) {
        return spec_icon_title_class(player.build.elite_spec, player.name + "\n" + player.build.name + "\nD/C: " + player.target_dps + "/" + player.target_cc, "icon-special");
    }
    else {
        return spec_icon_title(player.build.elite_spec, player.name + "\n" + player.build.name + "\nD/C: " + player.target_dps + "/" + player.target_cc);
    }
}
function failed_attempts(failed_logs) {
    if (failed_logs.length === 0) {
        return "<h3>No failed attempts</h3>";
    }
    var html = "<h3>Failed attempts</h3>\n<table id='failed-attempts'><tr><th>Date</th><th>Duration</th>" +
        "<th class='left-align'>Alac</th><th class='left-align'>Quick</th><th class='left-align'>Other</th>" +
        "<th style='padding-left: 0.2em; text-align: left; width: 400px;'>Wipe Reason</th><th>Log</th></tr>\n";
    failed_logs.forEach(function (fail) {
        var data = fail.data;
        var url = fail.url;
        var reason = "?";
        if (url in wipe_reasons) {
            reason = wipe_reasons[url];
        }
        var alac_players = fail.data.players.filter(function (p) { return p.build.elite_spec != "NoEliteSpec"; }).filter(function (p) {
            return p.group_buff_generation[ALACRITY_BUFF_ID] + p.off_group_buff_generation[ALACRITY_BUFF_ID] >= 30;
        }).sort(function (a, b) {
            return b.group_buff_generation[ALACRITY_BUFF_ID] - a.group_buff_generation[ALACRITY_BUFF_ID];
        });
        var alac_info = "";
        alac_players.forEach(function (p) {
            alac_info += player_in_log(p);
        });
        var quick_info = "";
        var quick_players = fail.data.players.filter(function (p) { return p.build.elite_spec != "NoEliteSpec"; }).filter(function (p) {
            return p.group_buff_generation[QUICKNESS_BUFF_ID] + p.off_group_buff_generation[QUICKNESS_BUFF_ID] >= 30;
        }).sort(function (a, b) {
            return -a.group_buff_generation[QUICKNESS_BUFF_ID] + b.group_buff_generation[QUICKNESS_BUFF_ID];
        });
        quick_players.forEach(function (p) {
            quick_info += player_in_log(p);
        });
        var other_info = "";
        var other_players = fail.data.players.filter(function (p) { return p.build.elite_spec != "NoEliteSpec"; });
        other_players = other_players.filter(function (p) {
            return !quick_players.some(function (qp) { return qp == p; }) && !alac_players.some(function (ap) { return ap == p; });
        });
        other_players.sort(function (p0, p1) { return p1.target_dps - p0.target_dps; });
        other_players.forEach(function (p) {
            other_info += player_in_log(p);
        });
        var pad = data.duration / 1000 < 100 ? 6 : 7;
        var time_str = "".concat(String(data.duration / 1000).padEnd(pad, "0"), "s");
        html += table_row([
            "".concat(data.start_time.toLocaleDateString("de-de", { day: "2-digit", month: "2-digit", year: "2-digit" })),
            time_str,
            alac_info,
            quick_info,
            other_info,
            reason,
            "<a href='".concat(url, "'>Link</a>")
        ], [undefined, "class='right-align'", undefined, undefined, undefined, undefined, undefined]);
    });
    html += "</table>";
    return html;
}
function successful_attempts(suc_logs) {
    if (suc_logs.length === 0) {
        return "<h3>No successful attempts</h3>";
    }
    var html = "<h3>Successful attempts</h3>\n" +
        "<table id='successful-attempts-tbl'><tr>" +
        "<th onclick='sortTable(0, \"successful-attempts-tbl\")'>Date</th>" +
        "<th onclick='sortTable(1, \"successful-attempts-tbl\")'>Duration</th>" +
        "<th class='left-align'>Alac</th><th class='left-align'>Quick</th><th class='left-align'>Other</th>" +
        "<th>Log</th></tr>\n";
    suc_logs.forEach(function (suc) {
        var data = suc.data;
        var url = suc.url;
        var alac_players = suc.data.players.filter(function (p) { return p.build.elite_spec != "NoEliteSpec"; }).filter(function (p) {
            return p.group_buff_generation[ALACRITY_BUFF_ID] + p.off_group_buff_generation[ALACRITY_BUFF_ID] >= 30;
        }).sort(function (a, b) {
            return b.group_buff_generation[ALACRITY_BUFF_ID] - a.group_buff_generation[ALACRITY_BUFF_ID];
        });
        var alac_info = "";
        alac_players.forEach(function (p) {
            alac_info += player_in_log(p);
        });
        var quick_info = "";
        var quick_players = suc.data.players.filter(function (p) { return p.build.elite_spec != "NoEliteSpec"; }).filter(function (p) {
            return p.group_buff_generation[QUICKNESS_BUFF_ID] + p.off_group_buff_generation[QUICKNESS_BUFF_ID] >= 30;
        }).sort(function (a, b) {
            return -a.group_buff_generation[QUICKNESS_BUFF_ID] + b.group_buff_generation[QUICKNESS_BUFF_ID];
        });
        quick_players.forEach(function (p) {
            quick_info += player_in_log(p);
        });
        var other_info = "";
        var other_players = suc.data.players.filter(function (p) { return p.build.elite_spec != "NoEliteSpec"; });
        other_players = other_players.filter(function (p) {
            return !quick_players.some(function (qp) { return qp == p; }) && !alac_players.some(function (ap) { return ap == p; });
        });
        other_players.sort(function (p0, p1) { return p1.target_dps - p0.target_dps; });
        other_players.forEach(function (p) {
            other_info += player_in_log(p);
        });
        var pad = data.duration / 1000 < 100 ? 6 : 7;
        var time_str = "".concat(String(data.duration / 1000).padEnd(pad, "0"), "s");
        html += table_row([
            "".concat(data.start_time.toLocaleDateString("de-de", { day: "2-digit", month: "2-digit", year: "2-digit" })),
            time_str,
            alac_info,
            quick_info,
            other_info,
            "<a href='".concat(url, "'>Link</a>")
        ], [undefined, "class='right-align'", undefined, undefined, undefined, undefined, undefined]);
    });
    html += "</table>";
    return html;
}
function spec_icon_title_class(spec_name, title, clazz) {
    if (clazz == undefined) {
        return "<img src='img/".concat(spec_name, "_tango_icon_20px.png' alt='").concat(spec_name, "' title='").concat(title, "'/>");
    }
    else {
        return "<img src='img/".concat(spec_name, "_tango_icon_20px.png' alt='").concat(spec_name, "' title='").concat(title, "' class='").concat(clazz, "'/>");
    }
}
function spec_icon_title(spec_name, title) {
    return spec_icon_title_class(spec_name, title, undefined);
}
function spec_icon(spec_name) {
    return "<img src='img/".concat(spec_name, "_tango_icon_20px.png' alt='").concat(spec_name, "'/>");
}
function get_class_from_spec(spec) {
    switch (spec) {
        case "Firebrand":
        case "Dragonhunter":
        case "Willbender":
        case "Guardian":
            return "Guardian";
        case "Berserker":
        case "Spellbreaker":
        case "Bladesworn":
            return "Warrior";
        case "Scrapper":
        case "Holosmith":
        case "Mechanist":
        case "Engineer":
            return "Engineer";
        case "Druid":
        case "Soulbeast":
        case "Untamed":
            return "Ranger";
        case "Daredevil":
        case "Thief":
        case "Deadeye":
        case "Specter":
            return "Thief";
        case "Tempest":
        case "Weaver":
        case "Catalyst":
            return "Elementalist";
        case "Chronomancer":
        case "Mirage":
        case "Virtuoso":
            return "Mesmer";
        case "Reaper":
        case "Scourge":
        case "Harbinger":
            return "Necromancer";
        case "Herald":
        case "Renegade":
        case "Vindicator":
            return "Revenant";
        default:
            return "Unknown";
    }
}
function dps_builds(successful_logs) {
    var builds = {};
    var build_count = {};
    var dps_sum = {};
    var total_dps_max = 0;
    var dps_max = {};
    var cc_sum = {};
    var dpss = {};
    var html = "<h3>DPS Builds</h3>";
    for (var log_index in successful_logs) {
        var log = successful_logs[log_index];
        for (var player_index in log.data.players) {
            var player = log.data.players[player_index];
            var build = player.build;
            if (!build.alac && !build.quick) {
                if (!(build.name in build_count)) {
                    build_count[build.name] = 0;
                    dps_sum[build.name] = 0;
                    dps_max[build.name] = 0;
                    cc_sum[build.name] = 0;
                    builds[build.name] = build;
                    dpss[build.name] = [];
                }
                build_count[build.name] += 1;
                dps_sum[build.name] += player.target_dps;
                dpss[build.name].push(player.target_dps);
                cc_sum[build.name] += player.target_cc;
                if (player.target_dps > dps_max[build.name]) {
                    dps_max[build.name] = player.target_dps;
                }
                if (player.target_dps > total_dps_max) {
                    total_dps_max = player.target_dps;
                }
            }
        }
    }
    var table_id = "dps_tbl";
    html += "<table id='".concat(table_id, "'><tr><th style='width:170px;text-align:left;' onclick='sortTable(0,\"").concat(table_id, "\")'>Build</th>") +
        "<th onclick='sortTable(1,\"".concat(table_id, "\")' style=\"width: 25px;\">#</th>") +
        "<th onclick='sortTable(2,\"".concat(table_id, "\")'>DPS</th>") +
        "<th>DPS VIZ</th>" +
        "<th onclick='sortTable(4,\"".concat(table_id, "\")'>CC</th></tr>");
    for (var build_name in build_count) {
        html += table_row(["".concat(spec_icon(builds[build_name].elite_spec)).concat(build_name), build_count[build_name], "".concat(Math.round(dps_sum[build_name] / build_count[build_name])), dist_viz(dpss[build_name], 100, total_dps_max, get_class_from_spec(builds[build_name].elite_spec)), "".concat(Math.round(cc_sum[build_name] / build_count[build_name]))], [undefined, "class='right-align'", "class='right-align'", undefined,
            "class='right-align'"]);
    }
    html += "</table>";
    return html;
}
function get_group_off_group_uptime(source_player, players, buff_id) {
    var count_in = 0;
    var count_out = 0;
    var uptime_sum_in = 0;
    var uptime_sum_out = 0;
    for (var player_index in players) {
        var player = players[player_index];
        if (player.sub_group == source_player.sub_group) {
            uptime_sum_in += player.buff_uptime[buff_id];
            count_in += 1;
        }
        else {
            uptime_sum_out += player.buff_uptime[buff_id];
            count_out += 1;
        }
    }
    return [uptime_sum_in / count_in, uptime_sum_out / count_out];
}
function create_graph_from_content(content, name, y_max, y_is_time) {
    var step;
    if (y_is_time) {
        if (y_max < 60 * 1000) //  1min
            step = 10 * 1000; // 10sec
        else if (y_max < 8 * 60 * 1000) //  8min
            step = 60 * 1000; //  1min
        else if (y_max < 20 * 60 * 1000) // 20min
            step = 2 * 60 * 1000; //  2min
        else if (y_max < 30 * 60 * 1000) // 30min
            step = 5 * 60 * 1000; //  5min
        else if (y_max < 60 * 60 * 1000)
            step = 10 * 60 * 1000;
        else if (y_max < 6 * 60 * 60 * 1000)
            step = 60 * 60 * 1000;
        else if (y_max < 24 * 60 * 60 * 1000)
            step = 6 * 60 * 60 * 1000;
        else
            step = 24 * 60 * 60 * 1000;
    }
    else {
        var mul = 1;
        step = 0;
        while (true) {
            if (y_max <= 35 * mul) {
                step = 5 * mul;
                break;
            }
            else if (y_max <= 70 * mul) {
                step = 10 * mul;
                break;
            }
            else if (y_max <= 150 * mul) {
                step = 20 * mul;
                break;
            }
            else {
                mul *= 10;
            }
        }
    }
    var steps = Math.floor(0.95 * y_max / step);
    var svg = "<svg width='400' height='310'>";
    for (var i = 1; i < steps + 1; i++) {
        var value = i * step;
        svg += "<line x1='37' x2='43' y1='".concat(10 + 280 - value / y_max * 280, "' y2='").concat(10 + 280 - value / y_max * 280, "' style='stroke:black;'/>");
        svg += "<line x1='40' x2='395' y1='".concat(10 + 280 - value / y_max * 280, "' y2='").concat(10 + 280 - value / y_max * 280, "' style='stroke:black;stroke-width:0.3;'/>");
        var value_str = value.toString();
        if (!y_is_time && value >= 1e4) {
            value_str = Math.floor(value / 1e3).toString() + "k";
        }
        else if (y_is_time) {
            var secs = Math.round(value / 1000);
            value_str = Math.round(secs / 60) + ":" + String(secs % 60).padStart(2, "0");
        }
        svg += "<text text-anchor='end' x='36' y='".concat(10 + 280 - value / y_max * 280 + 5, "'>").concat(value_str, "</text>");
    }
    svg += content;
    svg += "<line x1='40' x2='40' y1='10' y2='290' style='stroke:black;'/>";
    svg += "<line x1='40' x2='45' y1='10' y2='17' style='stroke:black;'/>";
    svg += "<line x1='40' x2='35' y1='10' y2='17' style='stroke:black;'/>";
    svg += "<line x1='40' x2='400' y1='290' y2='290' style='stroke:black;'/>";
    svg += "<text style='font-size:1.2em' text-anchor='middle' x='220' y='20'>".concat(name, "</text>");
    svg += "</svg>";
    return svg;
}
function create_line_graph(y_data, name, y_is_time) {
    if (y_data.length === 0) {
        return "ERROR: NO DATA";
    }
    var content = "<polyline style='fill:none;stroke:#06617d;stroke-width:1.5' points='";
    var x = 40;
    var x_step = 350 / y_data.length;
    var y_max = 1;
    for (var i = 0; i < y_data.length; i++) {
        if (y_data[i] !== undefined) {
            y_max = y_data[i] - y_data[i]; // correct data type
            break;
        }
    }
    for (var i = 0; i < y_data.length; i++) {
        var data_point = y_data[i];
        if (data_point)
            y_max = Math.max(data_point, y_max);
    }
    for (var i = 0; i < y_data.length; i++) {
        var data_point = y_data[i];
        if (data_point) {
            content += "".concat(x, ",").concat(10 + 280 - data_point / y_max * 0.9 * 280, " ");
        }
        else {
            content += "'/><polyline style='fill:none;stroke:#06617d;stroke-width:1.5' points='";
        }
        x += x_step;
    }
    content += "'/>";
    content += "<line x1='400' x2='393' y1='290' y2='295' style='stroke:black;'/>";
    content += "<line x1='400' x2='393' y1='290' y2='285' style='stroke:black;'/>";
    content += "<text text-anchor='end' x='400' y='305'>weeks</text>";
    return create_graph_from_content(content, name, y_max / 0.9, y_is_time);
}
function boons_classes(successful_logs, buff_id, name) {
    var builds = {};
    var build_count = {};
    var dps_sum = {};
    var dps_max = {};
    var cc_sum = {};
    var uptime_sum = {};
    var total_dps_max = 0;
    var dpss = {};
    var html = "<h3>".concat(name, "</h3>");
    for (var log_index in successful_logs) {
        var log = successful_logs[log_index];
        for (var player_index in log.data.players) {
            var player = log.data.players[player_index];
            var build = player.build;
            var uptimes = get_group_off_group_uptime(player, log.data.players, buff_id);
            var uptime_g = uptimes[0];
            var uptime_og = uptimes[1];
            var gen_g = player.group_buff_generation[buff_id];
            var gen_og = player.off_group_buff_generation[buff_id];
            if (gen_g > 20) {
                if (!(build.name in build_count)) {
                    build_count[build.name] = 0;
                    dps_sum[build.name] = 0;
                    dps_max[build.name] = 0;
                    cc_sum[build.name] = 0;
                    uptime_sum[build.name] = 0;
                    builds[build.name] = build;
                    dpss[build.name] = [];
                }
                build_count[build.name] += 1;
                dps_sum[build.name] += player.target_dps;
                cc_sum[build.name] += player.target_cc;
                dpss[build.name].push(player.target_dps);
                if (player.target_dps > total_dps_max) {
                    total_dps_max = player.target_dps;
                }
                if (player.target_dps > dps_max[build.name]) {
                    dps_max[build.name] = player.target_dps;
                }
                if (gen_og > 20) {
                    uptime_sum[build.name] += (uptime_g + uptime_og) / 2.0; // todo unfair weight
                }
                else {
                    uptime_sum[build.name] += uptime_g;
                }
            }
        }
    }
    var table_id = "buff_".concat(buff_id, "_tbl");
    html += "<table id='".concat(table_id, "'><tr><th style='width:170px;text-align:left;' onclick='sortTable(0,\"").concat(table_id, "\")'>Build</th>") +
        "<th onclick='sortTable(1,\"".concat(table_id, "\")' style=\"width: 25px;\">#</th>") +
        "<th onclick='sortTable(2,\"".concat(table_id, "\")'>DPS</th>") +
        "<th>DPS VIZ</th>" +
        "<th onclick='sortTable(4,\"".concat(table_id, "\")'>CC</th>") +
        "<th onclick='sortTable(5,\"".concat(table_id, "\")'>Uptime*</th></tr>");
    for (var build_name in build_count) {
        html += table_row(["".concat(spec_icon(builds[build_name].elite_spec)).concat(build_name), build_count[build_name], "".concat(Math.round(dps_sum[build_name] / build_count[build_name])), dist_viz(dpss[build_name], 100, total_dps_max, get_class_from_spec(builds[build_name].elite_spec)), "".concat(Math.round(cc_sum[build_name] / build_count[build_name])), "".concat(Math.round(uptime_sum[build_name] / build_count[build_name] * 10) / 10, "%")], [undefined, "class='right-align'", "class='right-align'", "class='right-align'",
            "class='right-align'", "class='right-align'"]);
    }
    html += "</table>";
    html += "*Squad or Group depending on build and patch";
    return html;
}
function load() {
    var inpFA = document.getElementById('filter_after');
    var inpFB = document.getElementById('filter_before');
    inpFA.value = "2020-01-01";
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    inpFB.valueAsDate = tomorrow;
    document.getElementById('filter_after').onchange = generate;
    document.getElementById('filter_before').onchange = generate;
    document.getElementById("btn2021").onclick = function () {
        inpFA.value = "2021-01-01";
        inpFB.value = "2021-12-31";
        generate();
    };
    document.getElementById("btn2022").onclick = function () {
        inpFA.value = "2022-01-01";
        inpFB.value = "2022-12-31";
        generate();
    };
    document.getElementById("btnPOF").onclick = function () {
        inpFA.value = "2017-09-22";
        inpFB.value = "2022-02-27";
        generate();
    };
    document.getElementById("btnEOD").onclick = function () {
        inpFA.value = "2022-02-28";
        inpFB.valueAsDate = tomorrow;
        generate();
    };
    document.getElementById("btnALL").onclick = function () {
        inpFA.value = "2000-01-01";
        inpFB.value = "2050-01-01";
        generate();
    };
    document.getElementById("ckbCM").onchange = function (ev) {
        document.getElementById("ckbNM").checked = true;
        generate();
    };
    document.getElementById("ckbNM").onchange = function (ev) {
        document.getElementById("ckbCM").checked = true;
        generate();
    };
    generate();
}
function generate() {
    var after = document.getElementById('filter_after').valueAsDate;
    var before = document.getElementById('filter_before').valueAsDate;
    var allowCM = document.getElementById('ckbCM').checked;
    var allowNM = document.getElementById('ckbNM').checked;
    var suc_filtered = successful_logs.filter(function (e) {
        return e.data.start_time > after && e.data.start_time < before && (allowCM && e.data.is_cm || allowNM && !e.data.is_cm);
    });
    var fail_filtered = failed_logs.filter(function (e) {
        return e.data.start_time > after && e.data.start_time < before && (allowCM && e.data.is_cm || allowNM && !e.data.is_cm);
    });
    var kill_times = Array.from(suc_filtered, function (e) { return e.data.duration; });
    document.getElementById("kill-time-diagram").innerHTML = create_line_graph(kill_times, "Kill time", true);
    document.getElementById("fail-attempts").innerHTML = failed_attempts(fail_filtered);
    document.getElementById("successful-attempts").innerHTML = successful_attempts(suc_filtered);
    document.getElementById("dps-builds").innerHTML = dps_builds(suc_filtered);
    document.getElementById("alac-builds").innerHTML = boons_classes(suc_filtered, ALACRITY_BUFF_ID, "Alacrity");
    document.getElementById("quick-builds").innerHTML = boons_classes(suc_filtered, QUICKNESS_BUFF_ID, "Quickness");
    sortTable(1, "buff_30328_tbl");
    sortTable(1, "buff_1187_tbl");
    sortTable(1, "dps_tbl");
}
